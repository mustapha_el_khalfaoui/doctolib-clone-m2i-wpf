﻿using AdminDoctolib.Models;
using AdminDoctolib.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AdminDoctolib.Views
{
    /// <summary>
    /// Logique d'interaction pour DetailPractitionerWindow.xaml
    /// </summary>
    public partial class DetailPractitionerWindow : Window
    {
        public DetailPractitionerWindow()
        {
            InitializeComponent();
        }
        public DetailPractitionerWindow(Practitioner p) : this()
        {
            DataContext = new DetailPractitionerWindowViewModel(p);
        }
    }
}
