﻿using AdminDoctolib.Models;
using AdminDoctolib.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AdminDoctolib.Views
{
    /// <summary>
    /// Logique d'interaction pour DetailPatientWindow.xaml
    /// </summary>
    public partial class DetailPatientWindow : Window
    {
        public DetailPatientWindow()
        {
            InitializeComponent();
        }

        public DetailPatientWindow(Patient p) : this()
        {
            DataContext = new DetailPatientWindowViewModel(p);
        }
    }
}
