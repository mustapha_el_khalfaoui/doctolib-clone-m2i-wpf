﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdminDoctolib.Migrations
{
    public partial class _20201219_1015 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_Patient_PatientId",
                table: "Appointment");

            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_Practitioners_PractitionerId",
                table: "Appointment");

            migrationBuilder.DropForeignKey(
                name: "FK_Practitioners_Address_AddressId",
                table: "Practitioners");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Patient",
                table: "Patient");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Appointment",
                table: "Appointment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Address",
                table: "Address");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Practitioners",
                table: "Practitioners");

            migrationBuilder.RenameTable(
                name: "Patient",
                newName: "patient");

            migrationBuilder.RenameTable(
                name: "Appointment",
                newName: "appointment");

            migrationBuilder.RenameTable(
                name: "Address",
                newName: "address");

            migrationBuilder.RenameTable(
                name: "Practitioners",
                newName: "practitioner");

            migrationBuilder.RenameIndex(
                name: "IX_Appointment_PractitionerId",
                table: "appointment",
                newName: "IX_appointment_PractitionerId");

            migrationBuilder.RenameIndex(
                name: "IX_Appointment_PatientId",
                table: "appointment",
                newName: "IX_appointment_PatientId");

            migrationBuilder.RenameIndex(
                name: "IX_Practitioners_AddressId",
                table: "practitioner",
                newName: "IX_practitioner_AddressId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_patient",
                table: "patient",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_appointment",
                table: "appointment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_address",
                table: "address",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_practitioner",
                table: "practitioner",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_appointment_patient_PatientId",
                table: "appointment",
                column: "PatientId",
                principalTable: "patient",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_appointment_practitioner_PractitionerId",
                table: "appointment",
                column: "PractitionerId",
                principalTable: "practitioner",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_practitioner_address_AddressId",
                table: "practitioner",
                column: "AddressId",
                principalTable: "address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_appointment_patient_PatientId",
                table: "appointment");

            migrationBuilder.DropForeignKey(
                name: "FK_appointment_practitioner_PractitionerId",
                table: "appointment");

            migrationBuilder.DropForeignKey(
                name: "FK_practitioner_address_AddressId",
                table: "practitioner");

            migrationBuilder.DropPrimaryKey(
                name: "PK_patient",
                table: "patient");

            migrationBuilder.DropPrimaryKey(
                name: "PK_appointment",
                table: "appointment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_address",
                table: "address");

            migrationBuilder.DropPrimaryKey(
                name: "PK_practitioner",
                table: "practitioner");

            migrationBuilder.RenameTable(
                name: "patient",
                newName: "Patient");

            migrationBuilder.RenameTable(
                name: "appointment",
                newName: "Appointment");

            migrationBuilder.RenameTable(
                name: "address",
                newName: "Address");

            migrationBuilder.RenameTable(
                name: "practitioner",
                newName: "Practitioners");

            migrationBuilder.RenameIndex(
                name: "IX_appointment_PractitionerId",
                table: "Appointment",
                newName: "IX_Appointment_PractitionerId");

            migrationBuilder.RenameIndex(
                name: "IX_appointment_PatientId",
                table: "Appointment",
                newName: "IX_Appointment_PatientId");

            migrationBuilder.RenameIndex(
                name: "IX_practitioner_AddressId",
                table: "Practitioners",
                newName: "IX_Practitioners_AddressId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Patient",
                table: "Patient",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Appointment",
                table: "Appointment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Address",
                table: "Address",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Practitioners",
                table: "Practitioners",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_Patient_PatientId",
                table: "Appointment",
                column: "PatientId",
                principalTable: "Patient",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_Practitioners_PractitionerId",
                table: "Appointment",
                column: "PractitionerId",
                principalTable: "Practitioners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Practitioners_Address_AddressId",
                table: "Practitioners",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
