﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdminDoctolib.Migrations
{
    public partial class _18122020_1510 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Practitioners",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Practitioners");
        }
    }
}
