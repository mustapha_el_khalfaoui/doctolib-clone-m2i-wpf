﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminDoctolib.Models
{
    [Table("practitioner")]
    public class Practitioner
    {
        private int id;
        private EnumStatus status;
        private string firstName;
        private string lastName;
        private EnumSpecialities speciality;
        private Address address;
        private List<Appointment> appointments;

        public int Id { get => id; set => id = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public EnumSpecialities Speciality { get => speciality; set => speciality = value; }
        public Address Address { get => address; set => address = value; }
        public EnumStatus Status { get => status; set => status = value; }
        public List<Appointment> Appointments { get => appointments; set => appointments = value; }

        public override string ToString()
        {
            return $"{FirstName} {LastName} {Speciality}";
        }
    }

    public enum EnumSpecialities
    {
        medecin,
        kine,
        ophtalmo
    }
    public enum EnumStatus
    {
        activated,
        unactivated
    }

}
