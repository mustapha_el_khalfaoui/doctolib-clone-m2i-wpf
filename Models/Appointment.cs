﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminDoctolib.Models
{
    [Table("appointment")]
    public class Appointment
    {
        private int id;
        private DateTime date;

        public int Id { get => id; set => id = value; }
        public DateTime Date { get => date; set => date = value; }

        [ForeignKey("Patient")]
        public int PatientId { get; set; }

        [ForeignKey("Practitioner")]
        public int PractitionerId { get; set; }

        public Patient Patient { get; set; }
        public Practitioner Practitioner { get; set; }

    }
}
