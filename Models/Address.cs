﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminDoctolib.Models
{
    [Table("address")]
    public class Address
    {
        private int id;
        private string postalAddress;
        private decimal lat;
        private decimal lng;

        public int Id { get => id; set => id = value; }
        public string PostalAddress { get => postalAddress; set => postalAddress = value; }
        public decimal Lat { get => lat; set => lat = value; }
        public decimal Lng { get => lng; set => lng = value; }

        public override string ToString()
        {
            return $"{PostalAddress}";
        }
    }
}
