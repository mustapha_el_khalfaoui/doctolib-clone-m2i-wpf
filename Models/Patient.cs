﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdminDoctolib.Models
{
    [Table("patient")]
    public class Patient
    {
        private int id;
        private string firstName;
        private string lastName;
        private string email;
        private string password;
        private List<Appointment> appointments;

        public int Id { get => id; set => id = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Email { get => email; set => email = value; }
        public string Password { get => password; set => password = value; }
        public List<Appointment> Appointments { get => appointments; set => appointments = value; }

        public override string ToString()
        {
            return $"{FirstName} {LastName} {Email}";
        }
    }
}
