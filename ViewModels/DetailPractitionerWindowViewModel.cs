﻿using AdminDoctolib.Models;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminDoctolib.ViewModels
{
    class DetailPractitionerWindowViewModel : ViewModelBase
    {
        private DataDbContext data;
        private Practitioner practitioner;

        public string FirstName
        {
            get => practitioner.FirstName;
            set
            {
                practitioner.FirstName = value;
                RaisePropertyChanged();
            }
        }
        public string LastName
        {
            get => practitioner.LastName;
            set
            {
                practitioner.LastName = value;
                RaisePropertyChanged();
            }
        }
        public string PostalAddress
        {
            get => practitioner.Address.PostalAddress;
            set
            {
                practitioner.Address.PostalAddress = value;
                RaisePropertyChanged();
            }
        }

        public List<Appointment> Appointments
        {
            get => practitioner.Appointments;
            set
            {
                practitioner.Appointments = value;
                RaisePropertyChanged();
            }
        }

        public DetailPractitionerWindowViewModel(Practitioner p)
        {
            data = new DataDbContext();
            practitioner = p;
            Appointments = p.Appointments;
        }
    }
}
