﻿using AdminDoctolib.Models;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminDoctolib.ViewModels
{
    class DetailPatientWindowViewModel : ViewModelBase
    {
        private DataDbContext data;
        private Patient patient;

        public string FirstName
        {
            get => patient.FirstName;
            set
            {
                patient.FirstName = value;
                RaisePropertyChanged();
            }
        }
        public string LastName
        {
            get => patient.LastName;
            set
            {
                patient.LastName = value;
                RaisePropertyChanged();
            }
        }

        public string Email
        {
            get => patient.Email;
            set
            {
                patient.Email = value;
                RaisePropertyChanged();
            }
        }

        public List<Appointment> Appointments
        {
            get => patient.Appointments;
            set
            {
                patient.Appointments = value;
                RaisePropertyChanged();
            }
        }

        public DetailPatientWindowViewModel(Patient p)
        {
            data = new DataDbContext();
            patient = p;
            Appointments = p.Appointments;
        }
    }


}
