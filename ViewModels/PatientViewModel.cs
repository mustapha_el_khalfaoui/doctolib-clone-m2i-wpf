﻿using AdminDoctolib.Models;
using AdminDoctolib.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace AdminDoctolib.ViewModels
{
    class PatientViewModel : ViewModelBase
    {
        private DataDbContext data;
        private Patient patient;

        public string FirstName
        {
            get => patient.FirstName;
            set
            {
                patient.FirstName = value;
                RaisePropertyChanged();
            }
        }
        public string LastName
        {
            get => patient.LastName;
            set
            {
                patient.LastName = value;
                RaisePropertyChanged();
            }
        }
        public string Email
        {
            get => patient.Email;
            set
            {
                patient.Email = value;
                RaisePropertyChanged();
            }
        }
        public string Password
        {
            get => patient.Password;
            set
            {
                patient.Password = value;
                RaisePropertyChanged();
            }
        }

        public Patient SelectedPatient { get; set; }

        public string Search { get; set; }

        public ObservableCollection<Patient> Patients { get; set; }

        public ICommand SearchCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DetailCommand { get; set; }
        public ICommand ConfirmCommand { get; set; }

        public PatientViewModel()
        {
            data = new DataDbContext();
            patient = new Patient();
            SelectedPatient = new Patient();
            Patients = new ObservableCollection<Patient>(data.Patients.Include(p => p.Appointments).ThenInclude(a => a.Practitioner));
            SearchCommand = new RelayCommand(SearchPatient);
            EditCommand = new RelayCommand(Edit);
            DetailCommand = new RelayCommand(Detail);
            ConfirmCommand = new RelayCommand(Confirm);
        }

        private void Confirm()
        {

            if (patient.Id == 0)
            {
                if (patient.FirstName != null && patient.LastName != null && patient.Email != null && patient.Password != null)
                {
                    data.Patients.Add(patient);
                    if (data.SaveChanges() > 0)
                    {
                        MessageBox.Show("Patient add");
                        Patients.Add(patient);
                        patient = new Patient();
                        RaisePropertyChanged("FirstName");
                        RaisePropertyChanged("LastName");
                        RaisePropertyChanged("Email");
                        RaisePropertyChanged("Password");
                    }
                }
                else
                {
                    MessageBox.Show("Fill all inputs");
                }
            }
            else
            {
                data.Patients.Update(patient);
                if (data.SaveChanges() > 0)
                {
                    MessageBox.Show("Modification ok");
                    Patients.Remove(patient);
                    RaisePropertyChanged("Practitioners");
                    Patients.Add(patient);
                    patient = new Patient();
                    RaisePropertyChanged("FirstName");
                    RaisePropertyChanged("LastName");
                    RaisePropertyChanged("Email");
                    RaisePropertyChanged("Password");
                }
                RaisePropertyChanged("Patients");
                SelectedPatient = new Patient();
            }

        }

        private void Edit()
        {
            if (SelectedPatient.Id != 0)
            {
                patient = SelectedPatient;
                RaisePropertyChanged("FirstName");
                RaisePropertyChanged("LastName");
                RaisePropertyChanged("Email");
                RaisePropertyChanged("Password");
            }
            else
            {
                MessageBox.Show("Select a patient to edit");
            }
        }
        private void Detail()
        {
            if (SelectedPatient.Id != 0)
            {
                DetailPatientWindow d = new DetailPatientWindow(SelectedPatient);
                d.Show();
            }
            else
            {
                MessageBox.Show("Select a patient to view details");
            }
        }
        private void SearchPatient()
        {
            if (Search != null)
            {
                Patients = new ObservableCollection<Patient>(data.Patients.Where(p => p.FirstName.Contains(Search)));
                RaisePropertyChanged("Patients");
            }
            else
            {
                MessageBox.Show("Type a keywork to launch a Search");

            }
        }
    }
}
