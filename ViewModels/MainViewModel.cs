﻿using AdminDoctolib.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace AdminDoctolib.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public ICommand PractitionerCommand { get; set; }
        public ICommand PatientCommand { get; set; }

        public MainViewModel()
        {
            PractitionerCommand = new RelayCommand<StackPanel>((result) => {
                result.Children.Clear();
                result.Children.Add(new PractitionerControl());
            });
            PatientCommand = new RelayCommand<StackPanel>((result) => {
                result.Children.Clear();
                result.Children.Add(new PatientControl());
            });
        }
    }
}
