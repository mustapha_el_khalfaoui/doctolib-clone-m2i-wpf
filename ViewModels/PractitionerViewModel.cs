﻿using AdminDoctolib.Models;
using AdminDoctolib.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace AdminDoctolib.ViewModels
{
    class PractitionerViewModel : ViewModelBase
    {
        private DataDbContext data;
        private Practitioner practitioner;
        private bool filterSearch;

        public string FirstName
        {
            get => practitioner.FirstName;
            set
            {
                practitioner.FirstName = value;
                RaisePropertyChanged();
            }
        }
        public string LastName
        {
            get => practitioner.LastName;
            set
            {
                practitioner.LastName = value;
                RaisePropertyChanged();
            }
        }
        public string PostalAddress
        {
            get => practitioner.Address.PostalAddress;
            set
            {
                practitioner.Address.PostalAddress = value;
                RaisePropertyChanged();
            }
        }

        public EnumSpecialities Speciality
        {
            get => practitioner.Speciality;
            set
            {
                practitioner.Speciality = value;
                RaisePropertyChanged();
            }
        }

        public EnumStatus CurrentStatus
        {
            get => practitioner.Status;
            set
            {
                practitioner.Status = value;
                RaisePropertyChanged();
            }
        }

        public List<EnumStatus> StatusList {get;set;}

        public Practitioner SelectedPractitioner { get; set; }
        public List<EnumSpecialities> Specialities { get; set; }

        public bool IsValid
        {
            get => SelectedPractitioner.Id != 0;
        }
        public string Search { get; set; }
        public ObservableCollection<Practitioner> Practitioners { get; set; }
        public ObservableCollection<Practitioner> TmpPractitioners { get; set; }

        public ICommand SearchCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DetailCommand { get; set; }
        public ICommand ConfirmCommand { get; set; }
        public ICommand FilterActivatedCommand { get; set; }


        public PractitionerViewModel()
        {
            data = new DataDbContext();
            practitioner = new Practitioner();
            SelectedPractitioner = new Practitioner();
            practitioner.Address = new Address();
            Practitioners = new ObservableCollection<Practitioner>(data.Practitioners.Include(p => p.Address).Include(p => p.Appointments).ThenInclude(a => a.Patient));
            TmpPractitioners = new ObservableCollection<Practitioner>();
            Specialities = new List<EnumSpecialities>(generateSpecialities());
            StatusList = new List<EnumStatus>(generateStatus());
            SearchCommand = new RelayCommand(SearchPractitioner);
            FilterActivatedCommand = new RelayCommand(FilterPractitioner);
            EditCommand = new RelayCommand(Edit);
            DetailCommand = new RelayCommand(Detail);
            ConfirmCommand = new RelayCommand(Confirm);
            filterSearch = false;
        }

        private List<EnumSpecialities> generateSpecialities()
        {
            return Enum.GetValues(typeof(EnumSpecialities))
                    .Cast<EnumSpecialities>().ToList();
        }
        private List<EnumStatus> generateStatus()
        {
            return Enum.GetValues(typeof(EnumStatus))
                    .Cast<EnumStatus>().ToList();
        }
        private void Confirm()
        {
            if (practitioner.Id == 0)
            {
                if (practitioner.FirstName != null && practitioner.LastName != null && practitioner.Address !=null)
                {
                    data.Practitioners.Add(practitioner);
                    if (data.SaveChanges() > 0)
                    {
                        MessageBox.Show("Practitioner add");
                        Practitioners.Add(practitioner);
                        practitioner = new Practitioner();
                        practitioner.Address = new Address();
                        RaisePropertyChanged("FirstName");
                        RaisePropertyChanged("LastName");
                        RaisePropertyChanged("PostalAddress");
                        RaisePropertyChanged("SelectedSpeciality");
                        RaisePropertyChanged("CurrentStatus");
                    }
                }
                else
                {
                    MessageBox.Show("Fill all inputs");
                }
            }
            else
            {
                data.Practitioners.Update(practitioner);
                if (data.SaveChanges() > 0)
                {
                    MessageBox.Show("Modification");
                    Practitioners.Remove(practitioner);
                    RaisePropertyChanged("Practitioners");
                    Practitioners.Add(practitioner);
                    practitioner = new Practitioner();
                    practitioner.Address = new Address();
                    RaisePropertyChanged("FirstName");
                    RaisePropertyChanged("LastName");
                    RaisePropertyChanged("PostalAddress");
                    RaisePropertyChanged("SelectedSpeciality");
                    RaisePropertyChanged("CurrentStatus");
                }
                RaisePropertyChanged("Practitioners");
                SelectedPractitioner = new Practitioner();
                RaisePropertyChanged("IsValid");
            }
        }

        private void Edit()
        {
            if (SelectedPractitioner.Id != 0)
            {
                practitioner = SelectedPractitioner;
                RaisePropertyChanged("FirstName");
                RaisePropertyChanged("LastName");
                RaisePropertyChanged("PostalAddress");
                RaisePropertyChanged("Speciality");
                RaisePropertyChanged("CurrentStatus");
                RaisePropertyChanged("IsValid");
            }
            else
            {
                MessageBox.Show("Select a practitioner to edit");
            }
        }

        private void Detail()
        {
            if (SelectedPractitioner.Id != 0)
            {
                DetailPractitionerWindow d = new DetailPractitionerWindow(SelectedPractitioner);
                d.Show();
            }
            else
            {
                MessageBox.Show("Select a practitioner to view details");
            }
        }
        private void SearchPractitioner()
        {
            if (Search != null)
            {
                Practitioners = new ObservableCollection<Practitioner>(data.Practitioners.Include(p => p.Address).Where(p => p.FirstName.Contains(Search)));
                RaisePropertyChanged("Practitioners");
            }
            else
            {
                MessageBox.Show("Type a keywork to launch a Search");

            }
        }

        private void FilterPractitioner()
        {
            filterSearch = !filterSearch;
            if (filterSearch)
            {
                foreach (Practitioner p in Practitioners.ToList())
                {
                    if (p.Status != EnumStatus.activated)
                    {
                        Practitioners.Remove(p);
                        TmpPractitioners.Add(p);
                    }
                }
                RaisePropertyChanged("Practitioners");
                RaisePropertyChanged("TmpPractitioners");
            }
            else
            {
                foreach (Practitioner tp in TmpPractitioners.ToList())
                {
                    if (tp.Status != EnumStatus.activated)
                    {
                        Practitioners.Add(tp);
                        TmpPractitioners.Remove(tp);
                    }
                }
                RaisePropertyChanged("Practitioners");
                RaisePropertyChanged("TmpPractitioners");
            }
        }
    }
}
